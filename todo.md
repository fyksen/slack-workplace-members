# Todo

So little time, so much to do I rather spend my days with you

- [ ] Clean up that the def get_channel_id(token, channel_name): checks for the channel name every time a new user is added.  
- [ ] Log info about last online with using https://api.slack.com/apis/presence-and-status#user-presence #Annonsering
- [ ] Handle namechanges and changes to file
- [ ] Show deactivated accounts in a separate view?
- [ ] Send notifications to slack channels
- [ ] Email subscriptions?
- [ ] Set hard dependencies in requirements file
- [ ] Testing

---

- [x] Retrieve what G_ groups user are member off, `terskel-g1` - `terskel-g4-1`
- [x] RSS feed
- [x] ENV variable for token
- [x] Move members.json to ./data dir
- [x] Create container image
- [x] Handle webhooks from slack, to get faster updates