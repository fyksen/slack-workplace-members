from flask import Flask, render_template, request, Response, jsonify
from feedgen.feed import FeedGenerator
from pytz import utc
import requests
import json
import datetime
import os
import threading
import time
import logging
import sys

app = Flask(__name__)

# Constants for file path
DATA_DIR = 'data'
MEMBERS_FILE = os.path.join(DATA_DIR, 'members.json')
SETTINGS_FILE = os.path.join(DATA_DIR, 'settings.json')
# Variable to store Slack Channel ID for notification:
channel_id_global = None
# Variable to store slack token:
token = os.getenv('SLACK_TOKEN')

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[logging.StreamHandler(sys.stdout)])

def load_settings():
    with open(SETTINGS_FILE, 'r') as file:
        return json.load(file)

def load_users_from_json(file_path=MEMBERS_FILE):
    try:
        if os.path.exists(file_path) and os.path.getsize(file_path) > 0:
            with open(file_path, 'r') as file:
                try:
                    return json.load(file)
                except json.JSONDecodeError:
                    logging.error("JSON decode error when loading users from file.")
                    return {}
        else:
            return {}
    except Exception as e:
        logging.error(f"Error reading from JSON file: {e}")
        return {}

def save_users_to_json(users, file_path=MEMBERS_FILE):
    try:
        with open(file_path, 'w') as file:
            json.dump(users, file, indent=4)
    except Exception as e:
        logging.error(f"Error writing to JSON file: {e}")

def fetch_and_update_users():
    with app.app_context():  # Ensure all parts of this function have access to Flask's context if needed
        existing_users = load_users_from_json()
        if not token:
            logging.error("SLACK_TOKEN is not set in the environment variables.")
            return

        url = 'https://slack.com/api/users.list'
        headers = {'Authorization': f'Bearer {token}'}
        params = {'limit': 200}
        new_users = False  # Flag to check if there are new or updated users

        try:
            while True:
                response = requests.get(url, headers=headers, params=params).json()

                if response.get('ok'):
                    for member in response.get('members', []):
                        if not member.get('is_bot', False):  # Exclude bot accounts
                            user_id = member['id']
                            is_deleted = member.get('deleted', False)
                            new_user_data = {
                                'id': user_id,
                                'email': member['profile'].get('email', 'No email provided'),
                                'name': member.get('name', 'No name provided'),
                                'real_name': member.get('real_name', 'No real name provided'),
                                'join_date': existing_users.get(user_id, {}).get('join_date', datetime.datetime.now().isoformat()),  # Preserve existing join_date or use current time
                                'deleted': is_deleted
                            }

                            # Check if this user is already recorded or if any details have changed
                            if user_id not in existing_users or existing_users[user_id] != new_user_data:
                                log_user_changes(existing_users.get(user_id, {}), new_user_data)
                                existing_users[user_id] = new_user_data
                                new_users = True  # Set flag as true if any new users are added or existing users are updated

                    # Handle pagination with next_cursor
                    next_cursor = response.get('response_metadata', {}).get('next_cursor')
                    if not next_cursor:
                        break
                    params['cursor'] = next_cursor
                else:
                    logging.error(f"Slack API returned an error: {response.get('error')}")
                    break

            if new_users:
                save_users_to_json(existing_users)

        except requests.exceptions.RequestException as e:
            logging.error(f"Request failed: {e}")
        except Exception as e:
            logging.error(f"Error during fetch and update process: {e}")

def log_user_changes(old_data, new_data):
    if old_data:
        changes = {key: {'old': old_data.get(key), 'new': new_data.get(key)} for key in new_data if old_data.get(key) != new_data.get(key)}
        if changes:
            logging.info(f"Changes for user {new_data['real_name']}: {changes}")
    else:
        logging.info(f"New user added: {new_data['real_name']}")


# Check channels names and gets channel IDs
def fetch_channel_ids(token):
    settings = load_settings()
    channel_names = set(settings['channels'])  # Use a set for faster lookups
    url = "https://slack.com/api/conversations.list"
    headers = {'Authorization': f'Bearer {token}'}
    params = {'limit': 200}  # Adjust as necessary

    channel_ids = {}
    while True:
        response = requests.get(url, headers=headers, params=params).json()
        if not response.get('ok'):
            logging.error(f"Failed to fetch channels: {response.get('error')}")
            break
        
        for channel in response.get('channels', []):
            if channel['name'] in channel_names:
                channel_ids[channel['name']] = channel['id']
                if len(channel_ids) == len(channel_names):  # Stop if all channels found
                    return channel_ids

        # Handle pagination
        next_cursor = response.get('response_metadata', {}).get('next_cursor')
        if not next_cursor:
            break
        params['cursor'] = next_cursor
    
    return channel_ids

# Retrieves members of channels
def fetch_channel_members(token, channel_id):
    with app.app_context():  # Ensuring context for Flask components
        url = f"https://slack.com/api/conversations.members"
        headers = {'Authorization': f'Bearer {token}'}
        params = {
            'channel': channel_id,
            'limit': 200  # You can adjust this value as necessary
        }

        members = []
        while True:
            response = requests.get(url, headers=headers, params=params).json()
            if response.get('ok'):
                members.extend(response.get('members', []))

                # Check for next_cursor and update the parameters if present
                next_cursor = response.get('response_metadata', {}).get('next_cursor')
                if not next_cursor:
                    break
                params['cursor'] = next_cursor
            else:
                logging.error(f"Failed to fetch channel members: {response.get('error')}")
                break

        return members

def update_memberships():
    with app.app_context():  # Ensuring context for Flask components
        users = load_users_from_json()
        channel_ids = fetch_channel_ids(token)
        logging.info(f"Channel IDs: {channel_ids}")


        for channel_name, channel_id in channel_ids.items():
            member_ids = fetch_channel_members(token, channel_id)
            for user_id in member_ids:
                if user_id in users:
                    if 'channels' not in users[user_id]:
                        users[user_id]['channels'] = []
                    if channel_name not in users[user_id]['channels']:
                        users[user_id]['channels'].append(channel_name)
        save_users_to_json(users)

def get_channel_id(token, channel_name):
    url = "https://slack.com/api/conversations.list"
    headers = {'Authorization': f'Bearer {token}'}
    params = {
        'limit': 200,
        'types': 'public_channel,private_channel'  # Request both public and private channels
    }

    while True:
        response = requests.get(url, headers=headers, params=params).json()
        if response.get('ok'):
            for channel in response.get('channels', []):
                if channel['name'] == channel_name:
                    logging.info(f"Found channel ID for {channel_name}: {channel['id']}")
                    return channel['id']

            next_cursor = response.get('response_metadata', {}).get('next_cursor')
            if not next_cursor:
                break
            params['cursor'] = next_cursor
        else:
            logging.error(f"Failed to fetch channels: {response.get('error')}")
            break

    logging.error(f"No channel found with the name {channel_name}")
    return None

def initialize_channel_id():
    channel_name = os.getenv('CHANNEL_NOTIFICATION')
    global channel_id_global
    channel_id_global = get_channel_id(token, channel_name)
    if channel_id_global:
        logging.info(f"Channel ID for notifications: {channel_id_global}")
    else:
        logging.error("Failed to retrieve channel ID at startup.")

@app.route('/event-subscription', methods=['POST'])
def event_subscription():
    data = request.json

    # Slack sends a challenge request to verify the endpoint
    if data.get('type') == 'url_verification':
        return jsonify({
            "challenge": data['challenge']
        })

    if data.get('type') == 'event_callback':
        if data['event']['type'] == 'team_join':
            handle_team_join_event(data['event']['user'])

    return jsonify({'status': 'OK'})

def handle_team_join_event(user):
    users = load_users_from_json()
    user_id = user['id']
    email = user['profile'].get('email', 'No email provided')
    real_name = user.get('real_name', 'No real name provided')

    if user_id not in users:
        users[user_id] = {
            'id': user_id,
            'email': email,
            'name': user.get('name', 'No name provided'),
            'real_name': real_name,
            'join_date': datetime.datetime.now().isoformat(),
            'deleted': user.get('deleted', False)
        }
        save_users_to_json(users)
        logging.info(f"Added new user {real_name} to the JSON file.")

        # Posting to Slack
        if channel_id_global:
            message = f"Welcome {real_name} ({email}) to the team!"
            post_message_to_channel(os.getenv('SLACK_TOKEN'), channel_id_global, message)
        else:
            logging.error("Channel ID not found, cannot post message.")

def post_message_to_channel(token, channel_id, message):
    url = "https://slack.com/api/chat.postMessage"
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-type': 'application/json'
    }
    payload = {
        'channel': channel_id,
        'text': message
    }
    response = requests.post(url, headers=headers, json=payload)
    response_data = response.json()
    if response_data.get('ok'):
        logging.info("Message posted successfully.")
    else:
        logging.error(f"Failed to post message: {response_data.get('error')}")

@app.route('/rss')
def generate_rss():
    fg = FeedGenerator()
    fg.title('New Slack Workspace Members')
    root_domain = os.getenv('ROOT_DOMAIN', 'http://localhost:5000')  # Default if not set
    fg.link(href=root_domain + request.path, rel='self')
    fg.description('Updates on new members joining the Slack workspace')

    users = load_users_from_json()
    active_users = [user for user in users.values() if not user.get('deleted', False)]

    # Convert join_date from string to datetime for proper sorting and ensure timezone is set
    for user in active_users:
        user['join_date'] = datetime.datetime.fromisoformat(user['join_date']).replace(tzinfo=utc)

    # Sort by join_date and then by id to ensure a stable sort if dates are identical
    recent_users = sorted(active_users, key=lambda x: (x['join_date'], x['id']), reverse=True)[:10]
    for details in recent_users:
        fe = fg.add_entry()
        fe.id(details['id'])
        fe.title(details['real_name'])
        fe.link(href=f"{root_domain}")  # Adjust if you have a direct link to user profiles
        fe.description(f"{details['real_name']} joined the Slack workspace.")
        fe.published(details['join_date'])  # This datetime now correctly includes timezone info

    response = fg.rss_str(pretty=True)  # Generate pretty RSS feed as a string
    return Response(response, content_type='application/rss+xml')

@app.route('/')
def index():
    sort_by = request.args.get('sort', 'join_date')
    order = request.args.get('order', 'desc')

    users = load_users_from_json()
    # Only pass non-deleted users to the template
    users_list = [user for user in users.values() if not user.get('deleted', True)]
    users_list.sort(key=lambda x: x[sort_by] if x[sort_by] is not None else "", reverse=order == 'desc')

    return render_template('users_list.html', users=users_list)

def update_data_periodically():
    while True:
        fetch_and_update_users()
        logging.info("Updated user data from Slack.")
        time.sleep(1200)  # Sleep for 20 minutes

def start_background_task():
    thread = threading.Thread(target=update_data_periodically)
    thread.daemon = True  # Daemonize thread to close with the main program
    thread.start()

def run_startup_tasks():
    if not os.getenv('WERKZEUG_RUN_MAIN'):  # This environment variable is true for the main process when the reloader is active
        logging.info("Skipping startup tasks in the reloader process.")
        return
    # Ensure the directory for members.json exists
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)
    # Ensure there is a members.json file with an initial empty JSON object if not existing or if it's empty
    if not os.path.exists(MEMBERS_FILE) or os.path.getsize(MEMBERS_FILE) == 0:
        save_users_to_json({})  # Initialize with an empty dictionary
    initialize_channel_id()  # Initialize channel ID at startup
    start_background_task()  # Start the background task

if __name__ == '__main__':
    run_startup_tasks()  # Run startup tasks that need to be executed once
    app.run(host="0.0.0.0", debug=True, port=5000)